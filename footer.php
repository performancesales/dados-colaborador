			
			<footer id="site-footer" role="contentinfo" >

			</footer><!-- #site-footer -->


			
		<?php wp_footer(); ?>

		<script>
			jQuery(document).ready(function($){
				
				var form = $('.wpcf7-form');

				// get saved data and populate fields
				if( form.length > 0 ){
					
					var data = localStorage.getItem('data'); 
					if( data ){ 
						
						data = JSON.parse(data);
					
						for (var key in data) {					
							
							if( key === 'veiculo-proprio' ){
								$("input[name=veiculo-proprio][value=" + data[key] + "]").prop('checked', true);
							} else {
								$('#' + key).val(data[key]);
							}
										
						}

					} 

				}


				// don't clear fields when submit
				$(document).on('reset', '.wpcf7-form', function(e) {
					e.preventDefault();
				});


				// save data to local storage on submit
				$(document).on('submit', '.wpcf7-form', function(e) {

					var data = {
						'empresa' : $('#empresa').val(),
						'nome-completo' : $('#nome-completo').val(),
						'nome-profissional' : $('#nome-profissional').val(),
						'genero' : $('#genero').val(),
						'morada-fiscal' : $('#morada-fiscal').val(),
						'morada-residencial' : $('#morada-residencial').val(),
						'numero-fiscal' : $('#numero-fiscal').val(),
						'identificacao' : $('#identificacao').val(),
						'numero-identificacao' : $('#numero-identificacao').val(),
						'data-validade' : $('#data-validade').val(),
						'seg-social' : $('#seg-social').val(),
						'carta-conducao' : $('#carta-conducao').val(),
						'veiculo-proprio' : $('input[name="veiculo-proprio"]:checked').val(),
						'matricula' : $('#matricula').val(),
						'data-nascimento' : $('#data-nascimento').val(),
						'naturalidade' : $('#naturalidade').val(),
						'nacionalidade' : $('#nacionalidade').val(),
						'estado-civil' : $('#estado-civil').val(),
						'titular-rendimentos' : $('#titular-rendimentos').val(),
						'numero-dependentes' : $('#numero-dependentes').val(),
						'habilitacoes-literarias' : $('#habilitacoes-literarias').val(),
						'contacto-pessoal' : $('#contacto-pessoal').val(),
						'email-pessoal' : $('#email-pessoal').val(),
						'contacto-emergencia' : $('#contacto-emergencia').val(),
						'iban' : $('#iban').val(),
						'bic-swift' : $('#bic-swift').val(),
					}

					localStorage.setItem('data', JSON.stringify(data));

				});

			});	
		</script>

	</body>
</html>
