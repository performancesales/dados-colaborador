<?php
    if( is_user_logged_in() ){
        wp_redirect( '/formulario' );
        exit;
    }
?>

<?php get_header(); ?>

<div class="login">
    
    <h2>Para visualizar o formulário, é necessário fazer login</h2>
    
    <?php wp_login_form( array( 'redirect' => '/formulario' ) ); ?>

    <a href="<?php echo wp_lostpassword_url() ?>">Esqueceu-se da senha?</a>
    
</div>


<?php get_footer(); ?>