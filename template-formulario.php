<?php
    if( !is_user_logged_in() ){
        wp_redirect( '/' );
        exit;
    }
?>

<?php get_header(); ?>

<?php /* Template Name: Formulário */ ?>

<div class="form-container">
<?php
	while ( have_posts() ) :
		the_post();

        ?>

        <div class="header-image" style="background-image:url('<?php echo get_template_directory_uri() ?>/images/fundo.jpg')"></div>
        
        <div class="form-group">
            <h1>Ficha de dados colaborador</h1>
            <p>Pedimos-te que preenchas esta ficha para avançar com o teu processo de entrada e/ou atualização dos teus dados:</p>
            <p class="required">*Obrigatório</p>
        </div>

        <?php

		the_content();

	endwhile; 
?>
</div>

<?php get_footer(); ?>