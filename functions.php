<?php

 
/**
 * First, let's set the maximum content width based on the theme's design and stylesheet.
 * This will limit the width of all uploaded images and embeds.
 */
if ( ! isset( $content_width ) )
    $content_width = 800; /* pixels */
 
if ( ! function_exists( 'theme_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which runs
 * before the init hook. The init hook is too late for some features, such as indicating
 * support post thumbnails.
 */
function theme_setup() {
 
    /**
     * Make theme available for translation.
     * Translations can be placed in the /languages/ directory.
     */
    load_theme_textdomain( 'theme' );
 
    /**
     * Add default posts and comments RSS feed links to <head>.
     */
    add_theme_support( 'automatic-feed-links' );
 
    /**
     * Enable support for post thumbnails and featured images.
     */
    add_theme_support( 'post-thumbnails' );
 
    /**
     * Add support for two custom navigation menus.
     */
   /* register_nav_menus( array(
        'primary'   => __( 'Primary Menu', 'theme' ),
        'secondary' => __('Secondary Menu', 'theme' )
    ) );*/
 
    /**
     * Enable support for the following post formats:
     * aside, gallery, quote, image, and video
     */
    add_theme_support( 'post-formats', array ( 'aside', 'gallery', 'quote', 'image', 'video' ) );
}
endif; // myfirsttheme_setup
add_action( 'after_setup_theme', 'theme_setup' );

/**
 * Register custom fonts.
 */
function twentyseventeen_fonts_url() {
	$fonts_url = '';

	/*
	 * translators: If there are characters in your language that are not supported
	 * by Libre Franklin, translate this to 'off'. Do not translate into your own language.
	 */
	$roboto = _x( 'on', 'Roboto font: on or off', 'theme' );

	if ( 'off' !== $roboto ) {
		$font_families = array();

		$font_families[] = 'Roboto:300,300i,400,400i,600,600i,800,800i';

		$query_args = array(
			'family'  => urlencode( implode( '|', $font_families ) ),
			'subset'  => urlencode( 'latin,latin-ext' ),
			'display' => urlencode( 'fallback' ),
		);

		$fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );
	}

	return esc_url_raw( $fonts_url );
}

add_filter( 'wpcf7_verify_nonce', '__return_true' );

/**
 * Enqueues scripts and styles.
 */
function add_theme_scripts() {
  
    wp_enqueue_style( 'style', get_stylesheet_uri() );
}
add_action( 'wp_enqueue_scripts', 'add_theme_scripts' );


add_filter('wpcf7_autop_or_not', '__return_false');


/**
 * Register "Colaboradores" post type
 */
 add_action( 'init',  'register_cpt_colaboradores'  );
function register_cpt_colaboradores(){
    $labels = array(
        "name" => __( "Colaboradores", "textdomain" ),
        "singular_name" => __( "Colaborador", "textdomain" ),
        "add_new" => __( "Adicionar Colaborador", "textdomain" ),
        "add_new_item" => __( "Adicionar novo Colaborador", "textdomain" ),
        "edit_item" => __( "Editar Colaborador", "textdomain" ),
        "new_item" => __( "Novo Colaborador", "textdomain" ),
    );

    $args = array(
        "label" => __( "Colaboradores", "textdomain" ),
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "delete_with_user" => false,
        "show_in_rest" => false, 
        "rest_base" => "",
        "rest_controller_class" => "WP_REST_Posts_Controller",
        "has_archive" => false,
        "show_in_menu" => true,
        "show_in_nav_menus" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "query_var" => true,
        "menu_icon" => "dashicons-groups",
        "supports" => array( "title" ),
    );

    register_post_type( "colaboradores", $args );
}


/**
 * Add colaboradores custom fields
 */
include_once( 'custom-fields.php' );


/**
 * Save submission to CPT "Colaboradores"
 */
add_action('wpcf7_mail_sent','save_form_data');
//add_action('wpcf7_mail_failed','save_form_data');
function save_form_data( $contact_form ){
    
    $submission = WPCF7_Submission::get_instance();
    if ( !$submission ){
        return;
    }

    $new_post = array(); // init new post array
    $posted_data = $submission->get_posted_data(); // get form data
    $current_user = wp_get_current_user(); // get current user

    $new_post['post_title'] = $current_user->user_email;
    $new_post['post_type'] = 'colaboradores';
    $new_post['post_status'] = 'publish';

    // If entry already exists, retrieve ID. Used to update instead
    if( $post_exists = get_page_by_title( $current_user->user_email, OBJECT, 'colaboradores' ) ){
        $new_post['ID'] = $post_exists->ID;
    } 

    // Create/update post
    if( $post_id = wp_insert_post( $new_post ) ){
        
        // loop posted data and update fields
        foreach( $posted_data as $key => $value ){

            if( is_array( $value ) ){
                $value = $value[0];
            }

            $key = str_replace( '-', '_', $key );
            update_field( $key, $value, $post_id );

        }
        
        // update submission date val
        update_field( 'ultima_submissao', date('Y-m-d'), $post_id );

        //$field = get_field_object('listing_type');
        //$field_key = $field['key'];

    }

    return;

}

/**
 * Create custom columns
 */
add_filter( 'manage_colaboradores_posts_columns', 'set_custom_edit_colaboradores_columns' );
function set_custom_edit_colaboradores_columns( $columns ) {
    
    $columns['name'] = __( 'Nome', 'theme' );
    $columns['company'] = __( 'Empresa', 'theme' );
    $columns['last_submission'] = __( 'Última submissão', 'theme' );
    $columns['export'] = __( 'Exportar', 'theme' );

    return $columns;
}

/**
 * Get value for custom column
 */
add_action( 'manage_colaboradores_posts_custom_column' , 'custom_colaboradores_column', 10, 2 );
function custom_colaboradores_column( $column, $post_id ) {
  switch ( $column ) {

    case 'name' :
        echo get_field( 'nome_completo', $post_id );  
        break;

    case 'company' :
        echo get_field( 'empresa', $post_id );  
        break;

    case 'last_submission' :
        echo get_field( 'ultima_submissao', $post_id );  
        break;
  
    case 'export' :
        echo '<input type="submit" name="colaborador[' . $post_id . ']" class="button button-primary" value="Exportar" />';  
        break;


  }
}

/**
 * Make custom column sortable
 */
add_filter( 'manage_edit-colaboradores_sortable_columns', 'set_custom_colaboradores_sortable_columns' );
function set_custom_colaboradores_sortable_columns( $columns ) {
    $columns['name'] = 'name';
    $columns['company'] = 'company';
    $columns['last_submission'] = 'last_submission';

    return $columns;
}

/**
 * Make custom column sortable
 */
add_action( 'pre_get_posts', 'colaboradores_custom_orderby' );
function colaboradores_custom_orderby( $query ) {
    if ( ! is_admin() )
        return;

    $orderby = $query->get( 'orderby');

    if ( 'name' == $orderby ) {
        $query->set( 'meta_key', 'nome_completo' );
        $query->set( 'orderby', 'meta_value' );
    }

    if ( 'company' == $orderby ) {
        $query->set( 'meta_key', 'empresa' );
        $query->set( 'orderby', 'meta_value' );
    }

    if ( 'last_submission' == $orderby ) {
        $query->set( 'meta_key', 'ultima_submissao' );
        $query->set( 'orderby', 'meta_value' );
    }

}

/**
 * Make colaboradores title read only
 */
function admin_footer_hook(){
    ?>
    <script type="text/javascript">
        if( jQuery('#post_type').val() === 'colaboradores' ){
            jQuery('#title').prop('disabled', true);
        }
    </script>
    <?php
}
add_action( 'admin_footer-post.php', 'admin_footer_hook' );

/**
 * Login redirect
 */
function admin_default_page() {
    return '/formulario';
}
add_filter('login_redirect', 'admin_default_page');


/**
 * Create colaboradores submenu page
 */
add_action( 'admin_menu', 'colaboradore_options_submenu' );
function colaboradore_options_submenu() {
  add_submenu_page(
        'edit.php?post_type=colaboradores',
        'Exportar',
        'Exportar',
        'administrator',
        'export_colaboradores',
        'colaboradore_options_page' 
    );
}

function colaboradore_options_page() { 
    
    $companies = array( "Bliss Aplication", "Bloom.Cast", "BYCOM", "Fever", "Nervo", "Performance Sales", "Plizz Solutions", "Processware", "Swolf", "W.Y.SGPS", "White Way" );
    
    ?>
    <div class="wrap">
        <h1>Exportar colaboradores</h1>
        <br>
        <form action="" method="post">
        

            <div>
                <label for="company">Exportar por empresa</label>
                <select name="company" id="company">
                    <option value="all">Todas</option>
                    <?php foreach ( $companies as $company ){ ?>
                        <option value="<?php echo $company ?>"><?php echo $company ?></option>
                    <?php } ?>
                </select>
            </div>
            <br>
            <input type="submit" name="export_colaboradores" class="button button-primary" value="Exportar" />

        </form>
        
    </div>
<?php } 

/**
 * Get post/get data for export
 */
function func_prepare_colaboradores_export() {

    if( isset( $_GET['colaborador'] )  ){
        $id = key ($_GET['colaborador'] );
        func_export_colaboradores( $id );      
    }

      
    if( isset( $_POST['export_colaboradores'] ) ){
        func_export_colaboradores();
    }

}
add_action( 'init', 'func_prepare_colaboradores_export' );


/**
 * Export CSV
 */
function func_export_colaboradores( $id = null ){

    $filename = "colaboradores.csv";

    $args = array(
        'post_type' => 'colaboradores',
        'post_status' => 'publish',
        'posts_per_page' => -1,
    );

    if( $id ){
        $args['p'] = $id;
        $args['posts_per_page'] = 1;
        $filename = "colaborador-" . strtolower( str_replace( ' ', '_',  get_field( 'nome_profissional', $id ) ) . ".csv" ); 
    }

    if( $_POST['company'] && $_POST['company'] !== 'all' ){
        $args['meta_key'] = 'empresa';
        $args['meta_value'] = $_POST['company'];
        $company = str_replace( '.', '', $_POST['company'] );
        $company = str_replace( ' ', '_', $_POST['company'] );
        $filename = "colaboradores-" . strtolower( $company ) . ".csv" ;
    }

    $query = new WP_Query( $args );
    if( $query->have_posts()  ){

        header('Content-type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename="' . $filename );
        header('Pragma: no-cache');
        header('Expires: 0');

        $file = fopen('php://output', 'w');
        fputs( $file, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));

        fputcsv($file, 
            array( 
                'E-mail', 
                'Empresa', 
                'Nome Completo', 
                'Nome Profissional', 
                'Género',
                'Morada FISCAL Completa',
                'Morada RESIDENCIAL Completa',
                'Número de Identificação Fiscal',
                'Documentação de Identificação',
                'Número do Documento de Identificação',
                'Data de Validade do Documento de Identificação',
                'Número de Identificação da Segurança Social',
                'Número da Carta de Condução',
                'Veículo Próprio',
                'Matrícula',
                'Data de Nascimento',
                'Naturalidade',
                'Nacionalidade',
                'Estado Civil',
                'Titularidade dos Rendimentos',
                'Número de Dependentes do Agregado Familiars',
                'Habilitações Literárias',
                'Contacto Pessoal',
                'E-mail pessoal',
                'Contacto em caso de urgência',
                'IBAN',
                'BIC/Swift',
                'Ultima Submissão'
            ) 
        );

   
        while( $query->have_posts() ){
            $query->the_post();
        
            $id = get_the_ID();

            $fields = array( 
                get_the_title(), 
                get_field( 'empresa', $id ),
                get_field( 'nome_completo', $id ), 
                get_field( 'nome_profissional', $id ), 
                get_field( 'genero', $id ), 
                get_field( 'morada_fiscal', $id ), 
                get_field( 'morada_residencial', $id ), 
                get_field( 'numero_fiscal', $id ), 
                get_field( 'identificacao', $id ), 
                get_field( 'numero_identificacao', $id ), 
                get_field( 'data_validade', $id ), 
                get_field( 'seg_social', $id ), 
                get_field( 'carta_conducao', $id ), 
                get_field( 'veiculo_proprio', $id ), 
                get_field( 'matricula', $id ), 
                get_field( 'data_nascimento', $id ), 
                get_field( 'naturalidade', $id ), 
                get_field( 'nacionalidade', $id ), 
                get_field( 'estado_civil', $id ), 
                get_field( 'titular_rendimentos', $id ), 
                get_field( 'numero_dependentes', $id ), 
                get_field( 'habilitacoes_literarias', $id ), 
                get_field( 'contacto_pessoal', $id ), 
                get_field( 'email_pessoal', $id ), 
                get_field( 'contacto_emergencia', $id ), 
                get_field( 'iban', $id ), 
                get_field( 'bic_swift', $id ), 
                get_field( 'ultima_submissao', $id )
            );
        
            fputcsv( $file, $fields );

        }

        exit();

        wp_reset_postdata();
    }
    

}